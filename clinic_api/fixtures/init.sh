#!/usr/bin/env bash
python3 manage.py loaddata clinic_api/fixtures/faculties.json \
&& python3 manage.py loaddata clinic_api/fixtures/departments.json \
&& python3 manage.py loaddata clinic_api/fixtures/groups.json \
&& python3 manage.py loaddata clinic_api/fixtures/students.json