from rest_framework import serializers

from clinic_api.models import Student, Fluorography, Vaccine, Group, \
    Department, Faculty, StudentVaccine


class FacultySerializer(serializers.ModelSerializer):
    class Meta:
        model = Faculty
        fields = ['id', 'name']


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ['id', 'name', 'faculty']


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'name', 'department']


class VaccineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vaccine
        fields = ['id', 'name', 'duration']


class FluorographySerializer(serializers.ModelSerializer):
    class Meta:
        model = Fluorography
        fields = ['id', 'date', 'result', 'student']


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ['id', 'last_name', 'first_name', 'middle_name', 'group']


class StudentVaccineSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentVaccine
        fields = ['id', 'student', 'vaccine', 'date', 'end_date']


class CustomSerializer(serializers.Serializer):
    last_name = serializers.CharField(max_length=200)
    first_name = serializers.CharField(max_length=200)
    middle_name = serializers.CharField(max_length=200)
    group__name = serializers.CharField(max_length=200)
    studentvaccine__date = serializers.DateField()
    studentvaccine__end_date = serializers.DateField()
    vaccines__name = serializers.CharField(max_length=200)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        instance.last_name = validated_data\
            .get('last_name', instance.last_name)
        instance.first_name = validated_data\
            .get('first_name', instance.irst_name)
        instance.middle_name = validated_data\
            .get('middle_name', instance.middle_name)
        instance.group__name = validated_data\
            .get('group_name', instance.group__name)
        instance.studentvaccine__date = validated_data\
            .get('date', instance.studentvaccine__date)
        instance.studentvaccine__end_date = validated_data\
            .get('end_date', instance.studentvaccine__end_date)
        instance.vaccines__name = validated_data\
            .get('vaccine_name', instance.vaccines__name)

        return instance
