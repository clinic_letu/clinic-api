from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from rest_framework.routers import DefaultRouter

from clinic_api.views import StudentModelViewSet, FacultyModelViewSet, \
    DepartmentModelViewSet, \
    GroupModelViewSet, VaccineModelViewSet, FluorographyModelViewSet, \
    StudentVaccineModelViewSet, VaccinatedStudentsViewSet, GenDocView

from clinic_api.views import DecoratedTokenRefreshView, \
    DecoratedTokenObtainPairView

router = DefaultRouter()

router.register(r'faculties', FacultyModelViewSet, basename='faculty')
router.register(r'departments', DepartmentModelViewSet, basename='department')
router.register(r'groups', GroupModelViewSet, basename='group')
router.register(r'vaccines', VaccineModelViewSet, basename='vaccine')
router.register(r'fluorographies', FluorographyModelViewSet, basename='fluorography')
# router.register(r'vacs', VaccinatedStudentsViewSet.as_view(), basename='vacs')
# router.register(r'students/<int:student_id>/vaccines', VaccinatedStudentsViewSet, basename='vaccinated-students')
router.register(r'students', StudentModelViewSet, basename='student')
router.register(r'student-vaccine', StudentVaccineModelViewSet, basename='student-vaccine')

urlpatterns = [
    # path('students/<int:student_id>/vaccines', VaccinatedStudentsViewSet.as_view({'get': 'list'})),
    path('gendoc/', GenDocView.as_view()),
    path('token/', DecoratedTokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('token/refresh/', DecoratedTokenRefreshView.as_view(),
         name='token_refresh')
]

urlpatterns += router.urls
