from datetime import date, datetime, timedelta
from io import BytesIO

import pandas as pd
from django.http import FileResponse
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, serializers, status
from rest_framework.filters import OrderingFilter
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenRefreshView, \
    TokenObtainPairView

from clinic_api.models import Student, Faculty, Department, Group, \
    Fluorography, Vaccine, StudentVaccine
from clinic_api.serializers import StudentSerializer, VaccineSerializer, \
    FluorographySerializer, StudentVaccineSerializer, GroupSerializer, \
    DepartmentSerializer, FacultySerializer, CustomSerializer


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 100


class FacultyModelViewSet(viewsets.ModelViewSet):
    """
    ModelViewSet для просмотра и редактирования сущностей факультетов
    """
    queryset = Faculty.objects.all()
    serializer_class = FacultySerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['name']


class DepartmentModelViewSet(viewsets.ModelViewSet):
    """
    ModelViewSet для просмотра и редактирования сущностей кафедр
    """
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['name', 'faculty']


class GroupModelViewSet(viewsets.ModelViewSet):
    """
    ModelViewSet для просмотра и редактирования сущностей групп
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['name', 'department']


class VaccineModelViewSet(viewsets.ModelViewSet):
    """
    ModelViewSet для просмотра и редактирования сущностей вакцин
    """
    queryset = Vaccine.objects.all()
    serializer_class = VaccineSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['name', 'duration']


class FluorographyModelViewSet(viewsets.ModelViewSet):
    """
    ModelViewSet для просмотра и редактирования сущностей флюорографий
    """
    queryset = Fluorography.objects.all()
    serializer_class = FluorographySerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['date', 'result', 'student']


class StudentModelViewSet(viewsets.ModelViewSet):
    """
    ModelViewSet для просмотра и редактирования сущностей студентов
    """
    queryset = Student.objects.all()
    # permission_classes = [permissions.IsAuthenticated]
    serializer_class = StudentSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['last_name', 'first_name', 'middle_name', 'group']


class StudentVaccineModelViewSet(viewsets.ModelViewSet):
    """
    ModelViewSet для просмотра и редактирования сущностей вакцин студентов
    """
    queryset = StudentVaccine.objects.all()
    serializer_class = StudentVaccineSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['student', 'vaccine', 'date', 'end_date']

    def create(self, request, *args, **kwargs):
        if request.data.get('date') and request.data.get('student') \
                and request.data.get('vaccine'):
            vaccine_date = datetime.strptime(
                request.data.get('date'),
                '%Y-%m-%d'
            ).date()
            vaccine_id = request.data['vaccine']
            duration_name = Vaccine.objects.get(pk=vaccine_id).duration.name
            duration_number, wmy = duration_name.split(' ')
            if 'месяц' in wmy:
                vaccine_date += timedelta(days=30 * int(duration_number))
            else:
                vaccine_date += timedelta(days=365 * int(duration_number))
            request.data['end_date'] = str(vaccine_date)
        else:
            request.data['end_date'] = '1970-01-01'

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=headers)


class VaccinatedStudentsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    ViewSet для просмотра вакцин студентов
    """
    # queryset = Vaccine.objects.filter(student=)
    serializer_class = VaccineSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    filterset_fields = ['name', 'duration']

    def get_queryset(self):
        return Vaccine.objects.filter(student=self.kwargs.get('student_id'))

    # def list(self, request, *args, **kwargs):
    #     queryset = self.get_queryset()
    #     filtered_queryset = self.filter_queryset(queryset)
    #     # filter_queryset
    #     _ = self.paginate_queryset(filtered_queryset)
    #
    #     serializer = self.get_serializer(filtered_queryset, many=True)
    #     response = self.get_paginated_response(serializer.data)
    #
    #     return response


def get_col_widths(df):
    col_width_arr = []
    for col in df.columns:
        maxim = 0
        for s in df[col].values:
            new_max = max([len(str(s))] + [len(col)])
            if new_max > maxim:
                maxim = new_max
        col_width_arr.append(maxim)

    return [x + 1 for x in col_width_arr]


class GenDocView(APIView):
    # Пока что GET
    def get(self, request):
        query = Student.objects.all().select_related(
            'group',
            'studentvaccine',
            'student__group',
        ).values(
            'last_name',
            'first_name',
            'middle_name',
            'group__name',
            'studentvaccine__date',
            'studentvaccine__end_date',
            'vaccines__name'
        ).order_by('last_name')

        group_id = request.query_params.get('group_id')
        doc_date = str(date.today())
        if group_id:
            query = query.filter(group__id=group_id)
            group_name = Group.objects.get(pk=group_id).name
            doc_name = f'Группа_{group_name}_Вакцинация_{doc_date}.xlsx'
        else:
            doc_name = f'Вакцинация_{doc_date}.xlsx'

        serializer = CustomSerializer(query, many=True)
        df = pd.DataFrame(serializer.data)
        df.rename(
            columns={
                'last_name': 'Фамилия',
                'first_name': 'Имя',
                'middle_name': 'Отчество',
                'group__name': 'Группа',
                'studentvaccine__date': 'Дата прививки',
                'studentvaccine__end_date': 'Дата окончания прививки',
                'vaccines__name': 'Вакцина'
            },
            inplace=True
        )

        file = BytesIO()

        writer = pd.ExcelWriter(file, engine='xlsxwriter')
        df.to_excel(excel_writer=writer, index=False)
        worksheet = writer.sheets['Sheet1']
        for i, width in enumerate(get_col_widths(df)):
            worksheet.set_column(i, i, width)
        writer.save()

        file.seek(0)

        return FileResponse(
            file,
            filename=doc_name,
            content_type='application/vnd.openxmlformats-officedocument'
                         '.spreadsheetml.sheet',
            status=status.HTTP_201_CREATED
        )


# TOKENS_FOR_SWAGGER
class TokenObtainPairResponseSerializer(serializers.Serializer):
    access = serializers.CharField()
    refresh = serializers.CharField()

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenObtainPairView(TokenObtainPairView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenObtainPairResponseSerializer
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class TokenRefreshResponseSerializer(serializers.Serializer):
    access = serializers.CharField()

    def create(self, validated_data):
        raise NotImplementedError()

    def update(self, instance, validated_data):
        raise NotImplementedError()


class DecoratedTokenRefreshView(TokenRefreshView):
    @swagger_auto_schema(
        responses={
            status.HTTP_200_OK: TokenRefreshResponseSerializer
        }
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)
