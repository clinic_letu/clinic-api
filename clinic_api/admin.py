from django.contrib import admin

from clinic_api.models import Student, Department, Faculty, Group, Vaccine, \
    Fluorography, StudentVaccine, VaccineDuration


@admin.register(Faculty)
class FacultyAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'faculty']


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'department']


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ['id', 'last_name', 'first_name', 'middle_name', 'group']


@admin.register(Vaccine)
class VaccineAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'duration']


@admin.register(StudentVaccine)
class StudentVaccineAdmin(admin.ModelAdmin):
    list_display = ['id', 'student', 'date', 'end_date', 'vaccine']


@admin.register(Fluorography)
class FluorographyAdmin(admin.ModelAdmin):
    list_display = ['id', 'date', 'result', 'student']


@admin.register(VaccineDuration)
class VaccineDurationAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']

