import datetime

from django.db.models import ForeignKey, Model, CASCADE, CharField, \
    ManyToManyField, TextChoices, DateField


class Faculty(Model):
    name = CharField(max_length=50)
    full_name = CharField(max_length=100)

    def __str__(self):
        return self.name


class Department(Model):
    name = CharField(max_length=50)
    full_name = CharField(max_length=100)
    faculty = ForeignKey(Faculty, on_delete=CASCADE)

    def __str__(self):
        return self.name


class Group(Model):
    name = CharField(max_length=10)
    department = ForeignKey(Department, on_delete=CASCADE)

    def __str__(self):
        return self.name


class VaccineDuration(Model):
    name = CharField(max_length=30)

    def __str__(self):
        return self.name


class Vaccine(Model):
    name = CharField(max_length=80)
    duration = ForeignKey(VaccineDuration, on_delete=CASCADE)

    def __str__(self):
        return self.name


class Student(Model):
    last_name = CharField(max_length=30)
    first_name = CharField(max_length=30)
    middle_name = CharField(max_length=30, null=True)
    group = ForeignKey(Group, on_delete=CASCADE)
    vaccines = ManyToManyField(Vaccine, through='StudentVaccine')

    def __str__(self):
        return '{} {} {}'.format(
            self.group.name,
            self.last_name,
            self.first_name
        )


class StudentVaccine(Model):
    student = ForeignKey(Student, on_delete=CASCADE)
    vaccine = ForeignKey(Vaccine, on_delete=CASCADE)
    date = DateField(default=datetime.date.today)
    end_date = DateField()

    def __str__(self):
        return '{} {} {}'.format(
            self.student.last_name,
            self.date,
            self.vaccine.name
        )


class Fluorography(Model):
    date = DateField(default=datetime.date.today)
    result = CharField(max_length=200)
    student = ForeignKey(Student, on_delete=CASCADE)

    def __str__(self):
        return ''.format()
