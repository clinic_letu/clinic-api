# Clinic API

## API
Доступные методы описаны в swagger по URL /swagger:

```
127.0.0.1:8000/swagger
```

## Docker Run
Для запуска проекта с помощью Docker используется следующая команда:

```
docker-compose up
```

