FROM python:3.10

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

RUN python3 manage.py makemigrations clinic_api \
&& python3 manage.py migrate \
&& python3 manage.py loaddata clinic_api/fixtures/faculties.json \
&& python3 manage.py loaddata clinic_api/fixtures/departments.json \
&& python3 manage.py loaddata clinic_api/fixtures/groups.json \
&& python3 manage.py loaddata clinic_api/fixtures/students.json

EXPOSE 8000

CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]